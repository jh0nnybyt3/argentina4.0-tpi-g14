$(document).ready(function () {

  // iniciar carousel
  $('.carousel').carousel({
    interval: 3000
  })


  // cagar equipo aleatorio
  $.ajax({
    url: "https://randomuser.me/api/?results=4",
    dataType: "json",
    success: function (data) {
      let listadoEquipo = $("#listado-equipo");
      $.each(data.results, function (index, persona) {
        let equipoHTML = `
            <div class="col-md-3">
              <div class="card">
                <img src="${persona.picture.large}" class="card-img-top" alt="Foto de ${persona.name.first} ${persona.name.last}">
                <div class="card-body">
                  <h5 class="card-title">${persona.name.first} ${persona.name.last}</h5>
                  <p class="card-text">${persona.email}</p>
                  <p class="card-text">Ingeniero</p>
                </div>
              </div>
            </div>
          `;
        listadoEquipo.append(equipoHTML);
      });
    },
    error: function (error) {
      console.error(error);
    },
  });

  // validar contacto
  $(function () {
    // Validación del formulario de contacto
    $("#formulario-contacto").validate({
      rules: {
        nombre: "required",
        email: {
          required: true,
          email: true,
        },
        mensaje: "required",
      },
      messages: {
        nombre: "Ingrese su nombre",
        email: {
          required: "Ingrese su correo electrónico",
          email: "Ingrese un correo electrónico válido",
        },
        mensaje: "Ingrese su mensaje",
      },
      errorElement: "span",
      submitHandler: function (formContacto) {
        // Envío del formulario de contacto
        var nombre = $("#nombre").val();

        // Mostrar el mensaje de éxito
        var success = "<p>Gracias por contactarnos, " + nombre + ".</p>";
        success += "<p>Nos pondremos en contacto contigo a la brevedad.</p>";
        $("#respuesta-contacto").html(success);

        $("#modalContacto").modal("show");

        // Limpiar el formulario
        formContacto.reset();
      },
    });
  });

  // activar el primer paso
  $(".tab-content > div:first-child").addClass("active");

  $("#pnombre-error").hide();
  $("#descripcion-error").hide();
  $("#material-error").hide();
  $("#color-error").hide();
  $("#tamano-error").hide();

  // siguiente
  $(".siguiente").click(function () {

    $("#pnombre-error").hide();
    $("#descripcion-error").hide();
    $("#material-error").hide();
    $("#color-error").hide();
    $("#tamano-error").hide();

    var siguiente = $(this).closest(".tab-pane").next(".tab-pane");
    var actual = $(this).closest(".tab-pane");
    var pnombre = $("#pNombre").val();
    var descripcion = $("#descripcion").val();
    var color = $("#color").val();
    var tamano = $("#tamano").val();
    var listMat = "";

    $("input:checkbox:checked").each(function () {
      listMat += $(this).val();
    });

    if (actual.prop("id") == "paso1") {
      if (pnombre != "" && descripcion != "") {
        $(this).closest(".tab-pane").removeClass("active");
        siguiente.addClass("active");
      } else {
        if (pnombre == "" && descripcion == "") {
          $("#pnombre-error").show();
          $("#descripcion-error").show();
        } else if (pnombre == "") {
          $("#pnombre-error").show();
        } else if (descripcion == "") {
          $("#descripcion-error").show();
        }
      }
    }
    if (actual.prop("id") == "paso2") {
      if (listMat != "") {
        $(this).closest(".tab-pane").removeClass("active");
        siguiente.addClass("active");
      } else {
          $("#material-error").show();
      }
    }
    if (actual.prop("id") == "paso3") {
      if (color != "" && tamano != "" && tamano > 0) {
        $(this).closest(".tab-pane").removeClass("active");
        siguiente.addClass("active");
      } else {
        if (color == "" && tamano == "" && tamano <= 0) {
          $("#color-error").show();
          $("#tamano-error").show();
        } else if (color == "") {
          $("#color-error").show();
        } else if (tamano == "" || tamano <= 0) {
          $("#tamano-error").show();
        }
      }
    }
  });

  // anterior
  $(".anterior").click(function () {
    var anterior = $(this).closest(".tab-pane").prev(".tab-pane");
    $(this).closest(".tab-pane").removeClass("active");
    anterior.addClass("active");
  });

  //comprobar cambios en los input cotizacion
  $(
    "#pNombre, #descripcion, #material-pla, material-abs, material-petg, #color, #tamano"
  ).change(function () {
    var pnombre = $("#pNombre").val();
    var descripcion = $("#descripcion").val();
    var color = $("#color").val();
    var tamano = $("#tamano").val();
    var listMat = "";

    $("input:checkbox:checked").each(function () {
      listMat += "<li>" + $(this).val() + "</li>";
    });

    $("#rNombre").text(pnombre);
    $("#rDescripcion").text(descripcion);
    $("#rMateirales").html(listMat);
    $("#rColor").text(color);
    $("#rTamano").text(tamano);
  });

  // enviar formulario
  $("#formulario-cotizacion").submit(function( event ) {
    event.preventDefault();

    generatePDF();

  });

  // generar PDF
  function generatePDF() {
    var pnombre = "Nombre: " + $("#pNombre").val();
    var descripcion = "Descripcion: " + $("#descripcion").val();
    var color = "Color: " + $("#color").val();
    var tamano = "Tamaño: " + $("#tamano").val() + " mm";
    var listMat = "Materiales: ";

    $("input:checkbox:checked").each(function () {
      listMat += " " + $(this).val();
    });

    var cotizacion = "Cotizacion " + $("#pNombre").val();

    var doc = new jsPDF();
    doc.setFontSize(20);
    doc.setFont("helvetica", "bold");
    doc.text(10, 20, cotizacion);
    doc.setFontSize(12);
    doc.setFont("helvetica", "normal");
    doc.text(10, 50, pnombre);
    doc.text(10, 60, descripcion);
    doc.text(10, 70, listMat);
    doc.text(10, 80, color);
    doc.text(10, 90, tamano);

    doc.save("cotizacion.pdf");
  }
});
